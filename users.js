let users = `

    <div class = "prev_next">
        <a href="javascript:prevPage()" class = "prev_next_btn" id="btn_prev">Prev</a>
        <a href="javascript:nextPage()" class = "prev_next_btn" id="btn_next">Next</a>
        <span class = "prev_next_btn" id="page"></span>
    </div>

	<div class="row">
			
					
		<div id="content">
					   			
		</div>			
					
        <div style="clear: both;"></div>
        <div id="listingTable"></div>
        </div>
			
    </div>

`;

let current_page = 1;
const records_per_page = 20;
let pageSize = records_per_page;

const githubUser = "getify";

const reppoAPI =
  "https://api.github.com/users/{user}/repos?page={page}&per_page=" +
  records_per_page;
const userInfoAPI = "https://api.github.com/users/{user}";

function repoEndoint(page) {
  return reppoAPI.replace("{user}", githubUser).replace("{page}", page);
}
function userEndpoint() {
  return reppoAPI.replace("{user}", githubUser);
}

function prevPage() {
  if (current_page > 1) {
    current_page--;
    changePage(current_page);
  }
}

function nextPage() {
  if (pageSize >= records_per_page) {
    current_page++;
    changePage(current_page);
  }
}

function getRepoData(page, filter) {
  fetch(repoEndoint(page))
    .then(function (response) {
      return response.json();
    })
    .then(function (data) {
      pageSize = data.length;

      data.forEach(function (item, index) {
        const el = document.getElementById("content");

        el.innerHTML += `
            <div class="item">
               <span>${item.full_name}<span>
              <img src = "${item.owner.avatar_url}" width = "96px"></img>
            </div>  
            `;
      });
    })
    .catch(function (err) {
      console.warn("Something went wrong.", err);
    });
}

function changePage(page) {
  const btn_next = document.getElementById("btn_next");
  const btn_prev = document.getElementById("btn_prev");
  const listing_table = document.getElementById("content");
  const page_span = document.getElementById("page");

  listing_table.innerHTML = "";
  getRepoData(page);

  page_span.innerHTML = page;

  if (page == 1) {
    btn_prev.style.visibility = "hidden";
  } else {
    btn_prev.style.visibility = "visible";
  }

  if (pageSize < records_per_page) {
    btn_next.style.visibility = "hidden";
  } else {
    btn_next.style.visibility = "visible";
  }
}

function numPages() {
  return 16;
}

window.onload = function () {
  changePage(1);
};
